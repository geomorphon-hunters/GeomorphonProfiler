# This file is a part of Geomorphon Profiler, a free software for geomorphon
# data processing. See the accompanying file "COPYING" for the copyright and
# licensing information.

import json
from sys import stderr
import os
from subprocess import run, DEVNULL
from tempfile import mkstemp
import argparse
import re
from typing import Tuple

DIRS = ['NE', 'N', 'NW', 'W', 'SW', 'S', 'SE', 'E']


def load_profile_or_exit (f, req_major: int, req_minor: int) -> dict:
	"""Load a geomorphon profile and check the format version"""
	try:
		ret = json.load (f)
	except json.decoder.JSONDecodeError as e:
		print ('Invalid JSON input: ' + str (e), file = stderr)
		exit (os.EX_DATAERR)
	MAJOR_KEY = 'format_version_major'
	MINOR_KEY = 'format_version_minor'
	if MAJOR_KEY not in ret or MINOR_KEY not in ret:
		print ('Unknown data format', file = stderr)
		exit (os.EX_DATAERR)
	if ret[MAJOR_KEY] != req_major or ret[MINOR_KEY] < req_minor:
		print ('Unsupported profile version %u.%u' %
			(ret[MAJOR_KEY], ret[MINOR_KEY]), file = stderr)
		exit (os.EX_DATAERR)
	return ret


def parse_coords (string: str) -> Tuple[float, float]:
	"""Convert a string to an (easting, northing) tuple."""
	m = re.match ('^([0-9.]+),([0-9.]+)$', string)
	if m is not None:
		return (float (m.group (1)), float (m.group (2)))
	raise argparse.ArgumentTypeError ('coordinates "%s" are invalid' % string)


class session:
	def __init__ (self):
		self.tmpfiles = {}
		try:
			proc = run ('g.version', stdout = DEVNULL, stderr = DEVNULL)
			in_grass = proc.returncode == 0
		except FileNotFoundError:
			in_grass = False
		if not in_grass:
			print ('ERROR: This script must be run in a GRASS GIS session.',
				file = stderr)
			exit (os.EX_USAGE)

	def create_tmp_files (self, map: list) -> dict:
		for pfx, sfx in map:
			fd, self.tmpfiles[pfx] = mkstemp (prefix = 'gptmp_%s_' % pfx, suffix = sfx)
			os.close (fd)
		return self.tmpfiles

	def remove_tmp_files (self):
		for fn in self.tmpfiles.values():
			if os.path.isfile (fn):
				os.remove (fn)
		self.tmpfiles = {}

	def run_command (self, cmdargs: list, quiet = False, input: bytes = None):
		try:
			kwargs = {'check': True}
			if input is not None:
				kwargs['input'] = input
			if quiet:
				kwargs['stdout'] = DEVNULL
				kwargs['stderr'] = DEVNULL
			run (cmdargs, **kwargs)
		except BaseException:
			print ('Failed to run command "%s"' % ' '.join (cmdargs), file = stderr)
			self.remove_tmp_files()
			exit (os.EX_OSERR)

	def __del__ (self):
		self.remove_tmp_files()
