#!/usr/bin/env python3

# This file is a part of Geomorphon Profiler, a free software for geomorphon
# data processing. See the accompanying file "COPYING" for the copyright and
# licensing information.

import argparse
import sys
import gplib

ap = argparse.ArgumentParser (
	description = 'Read elevation profiles along each of the 8 cardinal \
	directions from the provided geomorphon profile JSON file and output \
	them as a CSV file')
ap.add_argument (
	'--input',
	metavar = 'FILE',
	nargs = '?',
	type = argparse.FileType ('r'),
	default = sys.stdin,
	help = 'file to read the JSON data from ("-" for stdin)')
ap.add_argument (
	'--output',
	metavar = 'FILE',
	nargs = '?',
	type = argparse.FileType ('w'),
	default = sys.stdout,
	help = 'file to write the CSV data to ("-" for stdout)')
va = vars (ap.parse_args())

jsondata = gplib.load_profile_or_exit (va['input'], 0, 8)
for d in ['NE', 'N', 'NW', 'W', 'SW', 'S', 'SE', 'E']:
	vals = [d, '0.0']
	for f in jsondata['search_rel_elevation_m'][d].values():
		vals.append (str (f))
	print (','.join (vals), file = va['output'])
