#!/usr/bin/env python3

# This file is a part of Geomorphon Profiler, a free software for geomorphon
# data processing. See the accompanying file "COPYING" for the copyright and
# licensing information.

import argparse
import gplib


def parse_search (string: str) -> int:
	"""Convert a string to a r.geomorphon search radius value.

	Check that the value is within a sane range.
	"""
	search = int (string)
	if search >= 3 and search <= 10000:
		return search
	raise argparse.ArgumentTypeError ('search radius "%s" is invalid' % string)


def parse_arguments() -> dict:
	ap = argparse.ArgumentParser (description =
		'This script must be run in a GRASS GIS session. Given a \
		raster resolution, a search radius in cells and centre \
		coordinates, it computes the bounds of a sufficient \
		computational region by taking the smallest square that has \
		enough data for the r.geomorphon one-off computation and \
		adjusting the region edges outwards to be exactly on a cell \
		boundary to avoid raster resampling.')
	ap.add_argument (
		'--elevation',
		metavar = 'RASTER',
		required = True,
		help = 'r.geomorphon elevation raster name')
	ap.add_argument (
		'--visradius',
		metavar = 'UNITS',
		required = True,
		type = float,
		help = 'visualization radius in the current projection units \
			(i.e. degrees for LatLong, metres for NGR...)')
	ap.add_argument (
		'--search',
		metavar = 'CELLS',
		type = parse_search,
		help = 'r.geomorphon outer search radius')
	ap.add_argument (
		'--skip',
		metavar = 'CELLS',
		type = int,
		help = 'r.geomorphon inner search radius')
	ap.add_argument (
		'--flat',
		metavar = 'DEGREES',
		type = float,
		help = 'r.geomorphon flatness threshold')
	ap.add_argument (
		'--dist',
		metavar = 'CELLS',
		type = float,
		help = 'r.geomorphon flatness distance')
	ap.add_argument (
		'--coordinates',
		metavar = 'E,N',
		required = True,
		type = gplib.parse_coords,
		help = 'the point of interest ("easting,northing")')
	ap.add_argument (
		'--output',
		metavar = 'FILE',
		type = str,
		default = '-',
		help = 'file to write the JSON data to ("-" for stdout)')
	return vars (ap.parse_args())


va = parse_arguments()
session = gplib.session()

e, n = va['coordinates']
d = va['visradius']
session.run_command ([
	'g.region',
	'align=' + va['elevation'],
	'w=%f' % (e - d),
	'e=%f' % (e + d),
	's=%f' % (n - d),
	'n=%f' % (n + d),
])

opts2 = [
	'r.geomorphon',
	'--quiet',
	'--overwrite',
	'comparison=anglev2_distance',
	'coordinates=%f,%f' % va['coordinates'],
	'profiledata=' + va['output'],
	'profileformat=json',
]
for opt in ['elevation', 'search', 'skip', 'flat', 'dist']:
	if va[opt] is not None:
		opts2.append ('%s=%s' % (opt, str (va[opt])))

session.run_command (opts2)
