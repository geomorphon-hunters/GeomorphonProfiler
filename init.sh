#!/bin/sh

# This file is a part of Geomorphon Profiler, a free software for geomorphon
# data processing. See the accompanying file "COPYING" for the copyright and
# licensing information.

assert_nonempty_file()
{
	if ! [ -s "${1:?}" ]; then
		echo "ERROR: '$1' does not exist or is empty." >&2
		exit 1
	fi
}

mkdir_or_fail()
{
	if ! [ -d "${1:?}" ] && ! mkdir -p "$1"; then
		echo "Failed to create the directory '$1'." >&2
		exit 1
	fi
}

assert_dir()
{
	if ! [ -d "${1:?}" ]; then
		echo "ERROR: The directory '$1' does not exist." >&2
		exit 1
	fi
}

require_grass_session()
{
	if ! command -v g.version >/dev/null; then
		echo "ERROR: This script must be run in a GRASS GIS session." >&2
		exit 1
	fi
}

perf_start()
{
	PERF_START=$(date +%s)
}

div_scale2()
{
	echo "scale = 2; print ${1:?} / ${2:?}" | bc
}

format_elapsed_seconds()
{
	printf '%uh%02um%02us\n' \
		$((${1:?} / 3600)) $(((${1} % 3600) / 60)) $((${1} % 60))
}

perf_print()
{
	pp_count=${1:?}
	pp_interval=${2:?}
	if [ -z "$SQUELCH" ] && [ $((pp_count % pp_interval)) -eq 0 ]; then
		PP_NOW=$(date +%s)
		pp_elapsed=$((PP_NOW - PERF_START))
		# Avoid division by zero.
		[ $pp_elapsed -eq 0 ] && pp_elapsed=1
		pp_ips=$(div_scale2 "$pp_count" "$pp_elapsed")
		pp_spi=$(div_scale2 "$pp_elapsed" "$pp_count")
		pp_estr=$(format_elapsed_seconds "$pp_elapsed")
		echo "Performance after [$pp_estr]: $pp_ips items/s, $pp_spi s/item."
	fi
}

validate_integer()
{
	echo "${2:?}" | grep -q -E '^[[:digit:]]+$' && return 0
	echo "ERROR: ${1:?} must be an integer, invalid value '${2}'" >&2
	return 1
}

validate_float()
{
	echo "${2:?}" | grep -q -E '^[[:digit:]]+(\.[[:digit:]]+)?$' && return 0
	echo "ERROR: ${1:?} must be an integer or a float, invalid value '${2}'" >&2
	return 1
}

validate_config_verbatim()
{
	vcv_ret=0
	validate_integer SEARCH "$SEARCH" || vcv_ret=1
	validate_integer SKIP "$SKIP" || vcv_ret=1
	validate_float FLAT "$FLAT" || vcv_ret=1
	validate_float DIST "$DIST" || vcv_ret=1
	return $vcv_ret
}

# The default assignment works fine when other Geomorphon Profiler scripts
# source this file, but a 3rd-party script located in a different directory
# would need to set SCRIPTDIR before sourcing this file.
: "${SCRIPTDIR:=$(dirname "$(realpath "$0")")}"
if ! [ -f "$SCRIPTDIR/config.sh" ]; then
	echo "ERROR: config.sh not found" >&2
	echo "Copy config.sh.sample to config.sh and edit to complete the setup." >&2
	exit 1
fi
# shellcheck disable=SC1090
. "$SCRIPTDIR/config.sh"
: "${SEARCH:=50}" "${SKIP:=0}" "${FLAT:=1}" "${DIST:=0}"
: "${INPUTFILE:=$SCRIPTDIR/input.csv}"
if [ "$SHALLOW_INIT" != "yes" ]; then
	# If provided with an extra source, use it and run quietly.
	if [ -z "$1" ]; then
		SQUELCH=
	else
		assert_nonempty_file "$1"
		# shellcheck disable=SC1090
		. "$1"
		SQUELCH=:
	fi
	# Validate here because start.sh is not the only entry point.
	if ! validate_config_verbatim; then
		echo "Terminating due to a configuration problem." >&2
		echo "If the configuration is a matrix, use start.sh to process it." >&2
		exit 1
	fi

	ELEVDIR="${OUTPUTDIR:=$SCRIPTDIR/output}/${ELEVATION:?}"
	SE_SK_FL_DI="se${SEARCH}_sk${SKIP}_fl${FLAT}_di${DIST}"
	VI="vi${VIS_RADIUS:?}"
	# shellcheck disable=SC2034
	JSONDIR="$ELEVDIR/$SE_SK_FL_DI/profiles_$VI"
	# shellcheck disable=SC2034
	PNGDIR="$ELEVDIR/$SE_SK_FL_DI/images_$VI"
	# shellcheck disable=SC2034
	BACKDROPDIR="$ELEVDIR/cache/backdrops_$VI"
	# shellcheck disable=SC2034
	CONTOURDIR="$ELEVDIR/cache/contours_$VI"
	# shellcheck disable=SC2034
	SHAPEAREADIR="$ELEVDIR/$SE_SK_FL_DI/cache/shapeareas_$VI"
	# shellcheck disable=SC2034
	SHAPELINESDIR="$ELEVDIR/$SE_SK_FL_DI/cache/shapelines_$VI"
	# shellcheck disable=SC2034
	CIRCLEDIR="$ELEVDIR/$SE_SK_FL_DI/cache/circles_$VI"
fi
