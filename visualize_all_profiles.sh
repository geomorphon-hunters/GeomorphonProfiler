#!/bin/sh

# This file is a part of Geomorphon Profiler, a free software for geomorphon
# data processing. See the accompanying file "COPYING" for the copyright and
# licensing information.

# shellcheck source=./init.sh
. "$(dirname "$(realpath "$0")")/init.sh"
require_grass_session
assert_nonempty_file "$INPUTFILE"
assert_dir "$JSONDIR"
mkdir_or_fail "$PNGDIR"
mkdir_or_fail "$BACKDROPDIR"
mkdir_or_fail "$CONTOURDIR"
mkdir_or_fail "$SHAPEAREADIR"
mkdir_or_fail "$SHAPELINESDIR"
mkdir_or_fail "$CIRCLEDIR"

remove_tmp_layers()
{
	# Not entirely quiet if the element does not exist.
	g.remove --quiet -f type=raster pattern="${1:?}*" >/dev/null 2>&1
	g.remove --quiet -f type=vector pattern="${1:?}*" >/dev/null 2>&1
}

TMPFILE=$(mktemp --tmpdir visualization_XXXXXX.png)
TMPLAYERPFX='gpvistmp_'
remove_tmp_layers "$TMPLAYERPFX"
ret=0
doing=0
TODO=$(wc -l < "$INPUTFILE")
perf_start
while read -r line; do
	name=$(echo "$line" | cut -d, -f1)
	ecoords=$(echo "$line" | cut -d, -f4-5)
	[ "$ecoords" = ',' ] && ecoords=''
	doing=$((doing + 1))
	sourcefile="$JSONDIR/$name.json"
	targetfile="$PNGDIR/$name.png"
	$SQUELCH printf '%4u/%u: %s ' "$doing" "$TODO" "$targetfile"
	if ! [ -s "$sourcefile" ]; then
		echo 'ERROR: Source JSON file does not exist.' >&2
		ret=2
		break
	fi
	if [ -s "$targetfile" ]; then
		$SQUELCH echo 'already exists'
	else
		if [ -n "$SQUELCH" ] && [ -z "$REPORTED" ]; then
			printf 'visualizing... '
			REPORTED=yes
		fi
		$SQUELCH echo -n 'visualizing... '
		# Let VIS_OPTIONS expand.
		# shellcheck disable=SC2086
		if ! "$SCRIPTDIR/visualize_profile.py" \
			--input="$sourcefile" \
			--output="$TMPFILE" \
			$VIS_OPTIONS \
			${VIS_FINAL_COMMAND:+--final-command="$VIS_FINAL_COMMAND"} \
			${ecoords:+--coords-poi="$ecoords"} \
			--tmp-layer-prefix="$TMPLAYERPFX" \
			--backdrop-cache="$BACKDROPDIR/$name.png" \
			--contour-cache="$CONTOURDIR/$name.png" \
			--shape-area-cache="$SHAPEAREADIR/$name.png" \
			--shape-lines-cache="$SHAPELINESDIR/$name.png" \
			--circle-cache="$CIRCLEDIR/$name.png"
		then
			echo 'ERROR: Worker script failed.' >&2
			ret=1
			break
		fi
		cp "$TMPFILE" "$targetfile" && $SQUELCH echo 'done'
	fi
	perf_print $doing 100
done < "$INPUTFILE"
perf_print $doing 1
rm -f "$TMPFILE"
remove_tmp_layers "$TMPLAYERPFX"
exit $ret
