#!/usr/bin/env python3

# This file is a part of Geomorphon Profiler, a free software for geomorphon
# data processing. See the accompanying file "COPYING" for the copyright and
# licensing information.

import argparse
import sys
import gplib
import os
from shutil import copyfile
import re
import shlex
from threading import Thread
from collections import OrderedDict


class VisThread (Thread):
	def __init__ (self):
		super().__init__()

	def setContext (self, session: gplib.session, tmppfx: str):
		self.session = session
		self.tmppfx = tmppfx

	@staticmethod
	def usable (fn: str) -> bool:
		"""Tell whether the specified cache file is valid for reading"""
		return fn is not None and os.path.isfile (fn) and os.path.getsize (fn)

	@staticmethod
	def updatable (fn: str) -> bool:
		"""Tell whether the specified cache file is valid for writing"""
		return fn is not None

	@classmethod
	def assertUsable (cls, fn: str) -> str:
		if not cls.usable (fn):
			raise ValueError ('File "%s" is not usable!' % fn)
		return fn

	@classmethod
	def assertUpdatable (cls, fn: str) -> str:
		if not cls.updatable (fn):
			raise ValueError ('File "%s" is not updatable!' % fn)
		return fn

	def tmplayer (self):
		return '%s%u' % (self.tmppfx, self.ident)

	def r_out_png (self, raster_name: str, file_name: str,
		transparent: bool = True):
		"""Generate r.out.png options"""
		args = ['r.out.png', '--quiet', '--overwrite', 'compression=0']
		if transparent:
			args.append ('-t')
		args.extend (['input=' + raster_name, 'output=' + file_name])
		self.session.run_command (args)

	def r_contour (self, raster_name: str, file_name: str):
		self.session.run_command ([
			'r.contour', '--quiet',
			'input=' + raster_name,
			'step=10',
			'output=' + file_name,
		])

	def v_to_rast (self, vector_name: str, raster_name: str, densify: bool,
		cat: int):
		args = [
			'v.to.rast', '--quiet',
			'input=' + vector_name,
			'type=line',
			'use=val',
			'val=%u' % cat,
			'output=' + raster_name,
		]
		if densify:
			args.append ('-d')
		self.session.run_command (args)

	def r_in_poly (self, text: str, tmpfn: str, raster_name: str):
		print (text, end = '', file = open (tmpfn, 'w'))
		self.session.run_command ([
			'r.in.poly', '--quiet',
			'input=' + tmpfn,
			'output=' + raster_name,
		])

	def r_circle (self, coords: str, rmin: float, rmax: float, raster_name: str):
		self.session.run_command ([
			'r.circle', '--quiet',
			'-b',
			'coordinates=' + coords,
			'min=%f' % rmin,
			'max=%f' % rmax,
			'output=' + raster_name,
		])

	def r_colors (self, raster_name: str, cmap_name: str):
		"""Generate r.colors options"""
		self.session.run_command ([
			'r.colors',
			'--quiet',
			'map=' + raster_name,
			'color=' + cmap_name,
		])

	def g_remove (self, elem_type: str, elem_name: str):
		"""Generate g.remove options"""
		self.session.run_command ([
			'g.remove', '--quiet',
			'-f',
			'type=' + elem_type,
			'name=' + elem_name,
		])

	def composite (self, changefn: str, basefn: str, outputfn: str,
		extra_args: list = None):
		"""Run composite(1)"""
		args = [
			'composite',
			'-define', 'PNG:compression-level=0',
			'-define', 'PNG:compression-filter=0',
			'-define', 'PNG:compression-strategy=2',
		]
		if extra_args is not None:
			args.extend (extra_args)
		args.extend ([changefn, basefn, outputfn])
		self.session.run_command (args)

	def swap_colours (self, fn: str, old: str, new: str):
		"""Run mogrify(1) to replace one colour in an image with another"""
		self.session.run_command ([
			'mogrify',
			'-fill', new,
			'-opaque', old,
			'-define', 'PNG:compression-level=0',
			'-define', 'PNG:compression-filter=0',
			'-define', 'PNG:compression-strategy=2',
			fn,
		])


class ProduceCached (VisThread):
	def __init__ (self, cachefn: str, outputfn: str):
		self.cachefn = cachefn
		self.outputfn = self.assertUpdatable (outputfn)
		super().__init__()

	def run (self):
		if self.usable (self.cachefn):
			copyfile (self.cachefn, self.outputfn)
			return
		self.produce_output()
		if self.usable (self.outputfn) and self.updatable (self.cachefn):
			copyfile (self.outputfn, self.cachefn)


class ProduceCachedBackdrop (ProduceCached):
	def __init__ (self, raster: str, cachefn: str, outputfn: str):
		self.raster = raster
		super().__init__ (cachefn, outputfn)

	def produce_output (self):
		self.r_out_png (self.raster, self.outputfn, False)


class ProduceCachedContourLines (ProduceCached):
	def __init__ (self, raster: str, densify: bool, cat: int, cachefn: str,
		outputfn: str):
		self.raster = raster
		self.densify = densify
		self.cat = cat
		super().__init__ (cachefn, outputfn)

	def produce_output (self):
		self.r_contour (self.raster, self.tmplayer())
		self.v_to_rast (self.tmplayer(), self.tmplayer(), self.densify, self.cat)
		self.g_remove ('vector', self.tmplayer())
		self.r_colors (self.tmplayer(), 'grey1.0')
		self.r_out_png (self.tmplayer(), self.outputfn)
		self.g_remove ('raster', self.tmplayer())


class ProduceCachedRpoly (ProduceCached):
	def __init__ (self, tmpfn: str, text: str, cachefn: str, outputfn: str):
		self.tmpfn = self.assertUpdatable (tmpfn)
		self.text = text
		super().__init__ (cachefn, outputfn)

	def produce_output (self):
		self.r_in_poly (self.text, self.tmpfn, self.tmplayer())
		self.r_colors (self.tmplayer(), 'grey255')
		self.r_out_png (self.tmplayer(), self.outputfn)
		self.g_remove ('raster', self.tmplayer()),


class ProduceCachedShapeArea (ProduceCachedRpoly):
	"""From a polygon generate an area for r.in.poly"""
	def __init__ (self, tmpfn: str, boundary: list, cat: int, cachefn: str,
		outputfn: str):
		text = "A\n"
		for point in boundary:
			text += " %s %s\n" % point
		text += "= %u\n" % cat
		super().__init__ (tmpfn, text, cachefn, outputfn)


class ProduceCachedShapeLines (ProduceCachedRpoly):
	"""From a polygon generate a flat mesh for r.in.poly"""
	def __init__ (self, tmpfn: str, origin: tuple, boundary: list, cat: int,
		cachefn: str, outputfn: str):
		def gen_line (a: tuple, b: tuple, cat: int) -> str:
			ret = "L\n"
			ret += " %s %s\n" % a
			ret += " %s %s\n" % b
			ret += "= %u\n" % cat
			return ret

		text = ''
		# cardinal lines
		for point in boundary:
			text += gen_line (origin, point, cat)
		# boundary lines
		prev = boundary[len (boundary) - 1]
		for point in boundary:
			text += gen_line (prev, point, cat)
			prev = point
		super().__init__ (tmpfn, text, cachefn, outputfn)


class ProduceCircle (VisThread):
	def __init__ (self, coords: str, rmin: float, rmax: float, colour: str,
		outputfn: str):
		self.coords = coords
		self.rmin = rmin
		self.rmax = rmax
		self.colour = colour
		self.outputfn = self.assertUpdatable (outputfn)
		super().__init__()

	def run (self):
		self.r_circle (self.coords, self.rmin, self.rmax, self.tmplayer())
		self.r_colors (self.tmplayer(), 'grey1.0')
		self.r_out_png (self.tmplayer(), self.outputfn)
		self.g_remove ('raster', self.tmplayer())
		self.swap_colours (self.outputfn, '#ffffff', self.colour)


class ProduceCachedCircles (ProduceCached):
	def __init__ (self, circledefs: list, tmpfn: str, cachefn: str,
		outputfn: str):
		if len (circledefs) == 0:
			raise ValueError ('the list must not be empty')
		self.circledefs = circledefs
		self.tmpfn = self.assertUpdatable (tmpfn)
		super().__init__ (cachefn, outputfn)

	def produce_output (self):
		for t in self.circledefs:
			t.start()
		# For visual consistency composite in the order of thread spawning.
		for t in self.circledefs:
			t.join()
			if self.usable (self.outputfn):
				# composite in place
				self.composite (t.outputfn, self.outputfn, self.tmpfn)
				copyfile (self.tmpfn, self.outputfn)
			else:
				copyfile (t.outputfn, self.outputfn)


class JoinAndSwapColours (VisThread):
	def __init__ (self, predecessor: VisThread, old: str, new: str):
		self.predecessor = predecessor
		self.old = old
		self.new = new
		super().__init__()

	def run (self):
		self.predecessor.join()
		self.outputfn = self.assertUsable (self.predecessor.outputfn)
		self.swap_colours (self.outputfn, self.old, self.new)


class JoinAndComposite (VisThread):
	def __init__ (self, thread1: VisThread, thread2: VisThread, outputfn: str,
		composite_extra_args = None):
		self.thread1 = thread1
		self.thread2 = thread2
		self.composite_extra_args = composite_extra_args
		self.outputfn = self.assertUpdatable (outputfn)
		super().__init__()

	def run (self):
		self.thread1.join()
		self.thread2.join()
		self.composite (self.assertUsable (self.thread1.outputfn),
			self.assertUsable (self.thread2.outputfn),
			self.outputfn, self.composite_extra_args)


class JoinAndBlend (JoinAndComposite):
	def __init__ (self, thread1: VisThread, thread2: VisThread, outputfn: str,
		blend: str):
		super().__init__ (thread1, thread2, outputfn, ['-blend', blend])


class VisSession:
	def __init__ (self, session: gplib.session, tmppfx: str):
		self.session = session
		self.tmppfx = tmppfx

	def ready (self, cls, **kwargs) -> VisThread:
		ret = cls (**kwargs)
		ret.setContext (self.session, self.tmppfx)
		return ret

	def run (self, cls, **kwargs) -> VisThread:
		ret = self.ready (cls, **kwargs)
		ret.start()
		return ret


def parse_circle_params (s: str) -> tuple:
	"""Validate parameters for a circle"""
	m = re.match ('^([0-9.]+),([0-9.]+),(.+)$', s)
	if m is not None:
		return (float (m.group (1)), float (m.group (2)), m.group (3))
	raise argparse.ArgumentTypeError ('circle parameters "%s" are invalid' % s)


def parse_tmp_prefix (s: str) -> str:
	"""Validate a temporary layer prefix"""
	if re.match ('^[a-zA-Z0-9_]+$', s) is None:
		raise argparse.ArgumentTypeError ('value "%s" is not valid' % s)
	return s


ap = argparse.ArgumentParser (
	description = 'This script must be run in a GRASS GIS session. Given \
	a geomorphon profile produced with r.geomorphon one-off computation, \
	it visualizes the geomorphon and stores the result in a PNG file.')
ap.add_argument (
	'--input',
	metavar = 'FILE',
	nargs = '?',
	type = argparse.FileType ('r'),
	default = sys.stdin,
	help = 'file to read the JSON data from ("-" for stdin)')
ap.add_argument (
	'--output',
	metavar = 'FILE',
	nargs = '?',
	type = argparse.FileType ('wb'),
	default = sys.stdout.buffer,
	help = 'file to write the PNG data to ("-" for stdout)')
ap.add_argument (
	'--final-command',
	metavar = 'CMD',
	type = str,
	help = 'a custom command for the final image processing, for example, \
		"mogrify -resize 50%% -filter Catrom", "mogrify -scale 200%% \
		-density 600 -units PixelsPerInch" or "mogrify -gravity \
		SouthEast -watermark watermark.png"')
ap.add_argument (
	'--elevation-raster',
	metavar = 'NAME',
	type = str,
	help = 'GRASS raster to use for the elevation data instead of the \
		DEM raster name recorded in the geomorphon profile')
ap.add_argument (
	'--backdrop-raster',
	metavar = 'NAME',
	type = str,
	help = 'GRASS raster to use for the backdrop instead of the \
		elevation raster')
ap.add_argument (
	'--densify',
	action = 'store_true',
	help = 'densify the contour lines')
ap.add_argument (
	'--contour-colour',
	metavar = 'COLOUR',
	type = str,
	default = 'yellow',
	help = 'ImageMagick colour for the contour lines')
ap.add_argument (
	'--contour-blend',
	metavar = 'STR',
	type = str,
	default = '20%',
	help = 'composite(1) -blend geometry for the contour lines, for \
		example, "50%%" or "40x80%%"')
ap.add_argument (
	'--shape-area-colour',
	metavar = 'COLOUR',
	type = str,
	default = 'blue',
	help = 'ImageMagick colour for the shape octagon area')
ap.add_argument (
	'--shape-area-blend',
	metavar = 'STR',
	type = str,
	default = '20%',
	help = 'composite(1) -blend geometry for the shape octagon area')
ap.add_argument (
	'--shape-lines-colour',
	metavar = 'COLOUR',
	type = str,
	default = 'yellow',
	help = 'ImageMagick colour for the shape octagon area')
ap.add_argument (
	'--shape-lines-blend',
	metavar = 'STR',
	type = str,
	default = '30%',
	help = 'composite(1) -blend geometry for the shape octagon lines')
ap.add_argument (
	'--coords-poi',
	metavar = 'E,N',
	type = gplib.parse_coords,
	help = 'an optional extra point of interest ("easting,northing")')
ap.add_argument (
	'--circle-poi',
	metavar = 'STR',
	type = parse_circle_params,
	help = 'the extra POI circle parameters (min,max,colour), where \
		"min" and "max" means circle radius in metres as implemented \
		in r.circle(1) and "colour" is a value recognized by \
		ImageMagick')
ap.add_argument (
	'--circle-search',
	metavar = 'STR',
	type = parse_circle_params,
	help = 'the search radius circle parameters (min and max values are \
		relative to the search radius in metres')
ap.add_argument (
	'--circle-skip',
	metavar = 'STR',
	type = parse_circle_params,
	help = 'the skip circle parameters (min and max values are relative \
		to the skip distance in metres')
ap.add_argument (
	'--circle-minus',
	metavar = 'STR',
	type = parse_circle_params,
	help = 'circle parameters for the cardinal points that are below the \
		origin')
ap.add_argument (
	'--circle-zero',
	metavar = 'STR',
	type = parse_circle_params,
	help = 'circle parameters for the cardinal points that are at the \
		same elevation as the origin')
ap.add_argument (
	'--circle-plus',
	metavar = 'STR',
	type = parse_circle_params,
	help = 'circle parameters for the cardinal points that are above the \
		origin')
ap.add_argument (
	'--tmp-layer-prefix',
	metavar = 'STR',
	type = parse_tmp_prefix,
	default = 'gptmp',
	help = 'temporary GRASS layer prefix')
ap.add_argument (
	'--backdrop-cache',
	metavar = 'FILE',
	type = str,
	help = 'file to use as the backdrop read-write cache (the path must \
		be unique for the given combination of the input file and \
		the visualization radius value, the file will be created \
		if it does not exist)')
ap.add_argument (
	'--contour-cache',
	metavar = 'FILE',
	type = str,
	help = 'same as the above for the contour lines image')
ap.add_argument (
	'--shape-area-cache',
	metavar = 'FILE',
	type = str,
	help = 'same as the above for the geomorphon shape area image')
ap.add_argument (
	'--shape-lines-cache',
	metavar = 'FILE',
	type = str,
	help = 'same as the above for the geomorphon shape lines image')
ap.add_argument (
	'--circle-cache',
	metavar = 'FILE',
	type = str,
	help = 'same as the above for the superimposed circles image')
va = vars (ap.parse_args())
session = gplib.session()
vs = VisSession (session, va['tmp_layer_prefix'])
jsondata = gplib.load_profile_or_exit (va['input'], 0, 9)
session.run_command ([
	'g.region',
	'w=%s' % jsondata['map_info']['west'],
	'e=%s' % jsondata['map_info']['east'],
	's=%s' % jsondata['map_info']['south'],
	'n=%s' % jsondata['map_info']['north'],
])
elevation_raster = va['elevation_raster'] \
	if va['elevation_raster'] is not None \
	else jsondata['map_info']['elevation_name']
backdrop_raster = va['backdrop_raster'] \
	if va['backdrop_raster'] is not None \
	else elevation_raster

tmpmap = [
	('backdrop', '.png'),
	('contour', '.png'),
	('with_contour', '.png'),
	('rpoly_area', '.txt'),
	('shape_area', '.png'),
	('with_shape_area', '.png'),
	('rpoly_lines', '.txt'),
	('shape_lines', '.png'),
	('with_shape_lines', '.png'),
	('circle_search', '.png'),
	('circle_skip', '.png'),
	('circle_poi', '.png'),
	('composite_tmp', '.png'),
	('circles', '.png'),
	('final', '.png'),
]
tmpmap.extend ([('circle_' + d, '.png') for d in gplib.DIRS])
tmpfn = session.create_tmp_files (tmpmap)

origin = (
	jsondata['intermediate_data']['origin_easting'],
	jsondata['intermediate_data']['origin_northing']
)
boundary = OrderedDict()
for d in gplib.DIRS:
	boundary[d] = (
		jsondata['intermediate_data']['easting'][d],
		jsondata['intermediate_data']['northing'][d]
	)

backdrop = vs.run (
	ProduceCachedBackdrop,
	raster = backdrop_raster,
	cachefn = va['backdrop_cache'],
	outputfn = tmpfn['backdrop']
)

# The RGB values below derive from the integer values, i.e. 0 => "#000000",
# 1 => "#010101" and so on.
contour = vs.run (
	JoinAndSwapColours,
	predecessor = vs.run (
		ProduceCachedContourLines,
		raster = elevation_raster,
		densify = va['densify'],
		cat = 0,
		cachefn = va['contour_cache'],
		outputfn = tmpfn['contour']
	),
	old = '#000000',
	new = va['contour_colour']
)
shape_area = vs.run (
	JoinAndSwapColours,
	predecessor = vs.run (
		ProduceCachedShapeArea,
		tmpfn = tmpfn['rpoly_area'],
		boundary = list (boundary.values()),
		cat = 1,
		cachefn = va['shape_area_cache'],
		outputfn = tmpfn['shape_area']
	),
	old = '#010101',
	new = va['shape_area_colour']
)
shape_lines = vs.run (
	JoinAndSwapColours,
	predecessor = vs.run (
		ProduceCachedShapeLines,
		tmpfn = tmpfn['rpoly_lines'],
		origin = origin,
		boundary = list (boundary.values()),
		cat = 1,
		cachefn = va['shape_lines_cache'],
		outputfn = tmpfn['shape_lines']
	),
	old = '#010101',
	new = va['shape_lines_colour']
)

# Produce the optional circles, if any.
circledefs = []
circles = None
cmap = [
	('circle_search', jsondata['computation_parameters']['search_m']),
	('circle_skip', jsondata['computation_parameters']['skip_m']),
]
for key, rbase in cmap:
	if va[key] is not None:
		rmin, rmax, colour = va[key]
		circledefs.append (vs.ready (
			ProduceCircle,
			coords = '%s,%s' % origin,
			rmin = rbase + rmin,
			rmax = rbase + rmax,
			colour = colour,
			outputfn = tmpfn[key]
		))
cpmap = [
	(-1, 'circle_minus'),
	(0, 'circle_zero'),
	(1, 'circle_plus')
]
for delta, params in cpmap:
	if va[params] is not None:
		rmin, rmax, colour = va[params]
		for d, point in boundary.items():
			if jsondata['intermediate_data']['pattern'][d] == delta:
				circledefs.append (vs.ready (
					ProduceCircle,
					coords = '%s,%s' % point,
					rmin = rmin,
					rmax = rmax,
					colour = colour,
					outputfn = tmpfn['circle_' + d]
				))
if va['coords_poi'] is not None and va['circle_poi'] is not None:
	rmin, rmax, colour = va['circle_poi']
	circledefs.append (vs.ready (
		ProduceCircle,
		coords = '%s,%s' % va['coords_poi'],
		rmin = rmin,
		rmax = rmax,
		colour = colour,
		outputfn = tmpfn['circle_poi']
	))

# Composite the optional circles into a single image.
if len (circledefs):
	circles = vs.run (
		ProduceCachedCircles,
		circledefs = circledefs,
		tmpfn = tmpfn['composite_tmp'],
		cachefn = va['circle_cache'],
		outputfn = tmpfn['circles']
	)

# Superimpose the temporary contour lines.
with_contour = vs.run (
	JoinAndBlend,
	thread1 = contour,
	thread2 = backdrop,
	outputfn = tmpfn['with_contour'],
	blend = va['contour_blend']
)
# Superimpose the geomorphon shape layer.
with_shape_area = vs.run (
	JoinAndBlend,
	thread1 = shape_area,
	thread2 = with_contour,
	outputfn = tmpfn['with_shape_area'],
	blend = va['shape_area_blend']
)
with_shape_lines = vs.run (
	JoinAndBlend,
	thread1 = shape_lines,
	thread2 = with_shape_area,
	outputfn = tmpfn['with_shape_lines'],
	blend = va['shape_lines_blend']
)
# Superimpose the circles image.
if circles is not None:
	final = vs.run (
		JoinAndComposite,
		thread1 = circles,
		thread2 = with_shape_lines,
		outputfn = tmpfn['final']
	)
else:
	final = with_shape_lines
final.join()

if va['final_command'] is not None:
	args = shlex.split (va['final_command'])
	args.append (final.outputfn)
	session.run_command (args)

with open (final.outputfn, 'rb') as f:
	va['output'].write (f.read())
