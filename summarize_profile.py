#!/usr/bin/env python3

# This file is a part of Geomorphon Profiler, a free software for geomorphon
# data processing. See the accompanying file "COPYING" for the copyright and
# licensing information.

import argparse
import sys
import gplib

ap = argparse.ArgumentParser (description =
	'Read geomorphon profile data in JSON format and print a summary.')
ap.add_argument (
	'--input',
	metavar = 'FILE',
	nargs = '?',
	type = argparse.FileType ('r'),
	default = sys.stdin,
	help = 'file to read the JSON data from ("-" for stdin)')
ap.add_argument (
	'--output',
	metavar = 'FILE',
	nargs = '?',
	type = argparse.FileType ('w'),
	default = sys.stdout,
	help = 'file to write the summary to ("-" for stdout)')
va = vars (ap.parse_args())

jsondata = gplib.load_profile_or_exit (va['input'], 0, 8)
p = jsondata['intermediate_data']['pattern']
f = jsondata['final_results']
m = {-1: '-', 0: '0', 1: '+'}

print ('Landform: %u (%s, %s)' %
	(f['landform_cat'], f['landform_code'], f['landform_name']),
	file = va['output'])
print ('+--N--+', file = va['output'])
print ('|%c %c %c|' % (m[p['NW']], m[p['N']], m[p['NE']]), file = va['output'])
print ('|%c * %c|' % (m[p['W']], m[p['E']]), file = va['output'])
print ('|%c %c %c|' % (m[p['SW']], m[p['S']], m[p['SE']]), file = va['output'])
print ('+--S--+', file = va['output'])
