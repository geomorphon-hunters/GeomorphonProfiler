# Geomorphon Profiler

## What is it?
Geomorphon Profiler is an automation solution based on
[r.geomorphon](https://grass.osgeo.org/grass80/manuals/r.geomorphon.html),
[r.in.poly](https://grass.osgeo.org/grass80/manuals/r.in.poly.html),
[r.contour](https://grass.osgeo.org/grass80/manuals/r.contour.html),
[r.circle](https://grass.osgeo.org/grass80/manuals/r.circle.html), and
several other [GRASS GIS](https://grass.osgeo.org/) components, as well as
[ImageMagick](https://imagemagick.org/). It comprises shell and Python
scripts, and is a free software licensed under GPLv3.

## What problem does it address?
Given a list of points of interest (POI) and a matrix of r.geomorphon
parameters (search distance, skip radius, flatness threshold, and flatness
distance), Geomorphon Profiler iterates over every permutation of the
parameter values for each POI to produce the following results (see below for
examples):
* a PNG file with the geomorphon image (its shape octagon superimposed
  onto the relief elevation image with contour lines)
* a JSON file with the geomorphon profile (detailed data about the
  computation)

The user can extend Geomorphon Profiler (similar to as was done
[here](https://gitlab.com/geomorphon-hunters/place-name-props), for example)
to produce additional custom files from
the results, including CSV or PDF files with selected data elements for all
or some POIs; lists of SQL statements or HTTP requests; and so on.

Geomorphon Profiler automates this data processing and includes a convenient
option to stop or to resume it as required — in any scenario, each result
will be computed exactly once. As it uses a mixture of multithreading and
multiprocessing, overall performance scales up to a few CPU cores.

## What does it need to work?
* GNU/Linux (other POSIX-compatible operating systems should work too).
* ImageMagick (tested to work with 6.9).
* Python 3 (tested to work with 3.6 and 3.8).
* GRASS GIS version 8.0 or later.
* A pre-configured GRASS GIS location with a DEM (digital elevation model)
  raster layer.

## How to use it?
1. `cp config.sh.sample config.sh; $EDITOR config.sh`
2. Following the comments in the file, set `ELEVATION`, `VIS_RADIUS` and
   `SEARCH`; create `input.csv` with a POI list.
3. Start a GRASS GIS session.
4. Begin the processing with running `start.sh -v` in the session shell.
   You can stop the script using Ctrl-C to pause the processing and run it
   again to resume.
5. The results will start to appear in the `output` directory.

## Credits
* Dàibhidh Grannd, English Language and Linguistics, School of Critical
  Studies, University of Glasgow, 2020-2021 — the original scientific idea
  and testing.
* Denis Ovsienko, 2020-2023 — technical design and implementation.
* GRASS Development Team, 2022. [Geographic Resources Analysis Support
  System (GRASS) Software](https://grass.osgeo.org), Version 8.0.0 (DOI
  10.5281/zenodo.5913049).  Open Source Geospatial Foundation.
* The 30m DEM data used to generate the example results have been provided by
  [ALOS World 3D](https://www.eorc.jaxa.jp/ALOS/en/aw3d/index_e.htm) of the
  [Japan Aerospace Exploration Agency](https://global.jaxa.jp/).
* The ImageMagick Development Team, 2021. [ImageMagick](https://imagemagick.org).

## Example results

### Images of a single geomorphon
![NT 47197 56085](examples/NT_47197_56085.png)
![NT 47382 54797](examples/NT_47382_54797.png)
![NT 48595 56491](examples/NT_48595_56491.png)
![NT 53805 52953](examples/NT_53805_52953.png)
![NT 56141 56902](examples/NT_56141_56902.png)
![NT 56830 40378](examples/NT_56830_40378.png)
![NT 57478 46547](examples/NT_57478_46547.png)
![NT 59280 59201](examples/NT_59280_59201.png)
![NT 59425 53397](examples/NT_59425_53397.png)
![NT 71048 60473](examples/NT_71048_60473.png)
![NT 74545 62274](examples/NT_74545_62274.png)
![NT 75181 63003](examples/NT_75181_63003.png)
![NT 80874 66557](examples/NT_80874_66557.png)
![NT 81393 58074](examples/NT_81393_58074.png)
![NT 87664 64293](examples/NT_87664_64293.png)
![NT 89287 60119](examples/NT_89287_60119.png)

### Profile of a single geomorphon
```json
{
  "search_rel_elevation_m": {
    "NE": {
      "step_1": -2.00000000,
      "step_2": -2.00000000,
      "step_3": -2.00000000,
      "step_4": -1.00000000,
      "step_5": -1.00000000,
      "step_6": -2.00000000,
      "step_7": -3.00000000,
      "step_8": -6.00000000,
      "step_9": -9.00000000,
      "step_10": -13.00000000,
      "step_11": -17.00000000,
      "step_12": -20.00000000,
      "step_13": -25.00000000,
      "step_14": -29.00000000,
      "step_15": -34.00000000,
      "step_16": -35.00000000,
      "step_17": -34.00000000,
      "step_18": -32.00000000,
      "step_19": -33.00000000,
      "step_20": -39.00000000,
      "step_21": -43.00000000
    },
    "N": {
      "step_1": 0.00000000,
      "step_2": -1.00000000,
      "step_3": -2.00000000,
      "step_4": -2.00000000,
      "step_5": -4.00000000,
      "step_6": -5.00000000,
      "step_7": -7.00000000,
      "step_8": -10.00000000,
      "step_9": -14.00000000,
      "step_10": -18.00000000,
      "step_11": -22.00000000,
      "step_12": -25.00000000,
      "step_13": -28.00000000,
      "step_14": -30.00000000,
      "step_15": -32.00000000,
      "step_16": -33.00000000,
      "step_17": -32.00000000,
      "step_18": -32.00000000,
      "step_19": -32.00000000,
      "step_20": -32.00000000,
      "step_21": -32.00000000,
      "step_22": -32.00000000,
      "step_23": -31.00000000,
      "step_24": -32.00000000,
      "step_25": -33.00000000,
      "step_26": -34.00000000,
      "step_27": -34.00000000,
      "step_28": -32.00000000,
      "step_29": -30.00000000
    },
    "NW": {
      "step_1": -1.00000000,
      "step_2": -4.00000000,
      "step_3": -8.00000000,
      "step_4": -12.00000000,
      "step_5": -19.00000000,
      "step_6": -27.00000000,
      "step_7": -27.00000000,
      "step_8": -23.00000000,
      "step_9": -25.00000000,
      "step_10": -28.00000000,
      "step_11": -28.00000000,
      "step_12": -27.00000000,
      "step_13": -27.00000000,
      "step_14": -26.00000000,
      "step_15": -25.00000000,
      "step_16": -24.00000000,
      "step_17": -24.00000000,
      "step_18": -18.00000000,
      "step_19": -13.00000000,
      "step_20": -14.00000000,
      "step_21": -17.00000000
    },
    "W": {
      "step_1": -1.00000000,
      "step_2": -2.00000000,
      "step_3": -3.00000000,
      "step_4": -4.00000000,
      "step_5": -6.00000000,
      "step_6": -8.00000000,
      "step_7": -11.00000000,
      "step_8": -15.00000000,
      "step_9": -18.00000000,
      "step_10": -20.00000000,
      "step_11": -21.00000000,
      "step_12": -23.00000000,
      "step_13": -23.00000000,
      "step_14": -25.00000000,
      "step_15": -27.00000000,
      "step_16": -28.00000000,
      "step_17": -25.00000000,
      "step_18": -23.00000000,
      "step_19": -21.00000000,
      "step_20": -20.00000000,
      "step_21": -21.00000000,
      "step_22": -22.00000000,
      "step_23": -21.00000000,
      "step_24": -21.00000000,
      "step_25": -20.00000000,
      "step_26": -19.00000000,
      "step_27": -18.00000000,
      "step_28": -18.00000000,
      "step_29": -17.00000000
    },
    "SW": {
      "step_1": -1.00000000,
      "step_2": -2.00000000,
      "step_3": -4.00000000,
      "step_4": -7.00000000,
      "step_5": -10.00000000,
      "step_6": -13.00000000,
      "step_7": -16.00000000,
      "step_8": -19.00000000,
      "step_9": -21.00000000,
      "step_10": -23.00000000,
      "step_11": -26.00000000,
      "step_12": -30.00000000,
      "step_13": -33.00000000,
      "step_14": -37.00000000,
      "step_15": -40.00000000,
      "step_16": -43.00000000,
      "step_17": -44.00000000,
      "step_18": -45.00000000,
      "step_19": -45.00000000,
      "step_20": -46.00000000,
      "step_21": -47.00000000
    },
    "S": {
      "step_1": -1.00000000,
      "step_2": -2.00000000,
      "step_3": -4.00000000,
      "step_4": -6.00000000,
      "step_5": -9.00000000,
      "step_6": -11.00000000,
      "step_7": -14.00000000,
      "step_8": -16.00000000,
      "step_9": -19.00000000,
      "step_10": -22.00000000,
      "step_11": -25.00000000,
      "step_12": -28.00000000,
      "step_13": -31.00000000,
      "step_14": -33.00000000,
      "step_15": -35.00000000,
      "step_16": -37.00000000,
      "step_17": -40.00000000,
      "step_18": -41.00000000,
      "step_19": -42.00000000,
      "step_20": -44.00000000,
      "step_21": -45.00000000,
      "step_22": -45.00000000,
      "step_23": -46.00000000,
      "step_24": -47.00000000,
      "step_25": -50.00000000,
      "step_26": -51.00000000,
      "step_27": -53.00000000,
      "step_28": -55.00000000,
      "step_29": -56.00000000
    },
    "SE": {
      "step_1": -2.00000000,
      "step_2": -5.00000000,
      "step_3": -8.00000000,
      "step_4": -11.00000000,
      "step_5": -13.00000000,
      "step_6": -17.00000000,
      "step_7": -21.00000000,
      "step_8": -26.00000000,
      "step_9": -31.00000000,
      "step_10": -37.00000000,
      "step_11": -40.00000000,
      "step_12": -43.00000000,
      "step_13": -47.00000000,
      "step_14": -51.00000000,
      "step_15": -52.00000000,
      "step_16": -50.00000000,
      "step_17": -51.00000000,
      "step_18": -52.00000000,
      "step_19": -54.00000000,
      "step_20": -54.00000000,
      "step_21": -53.00000000
    },
    "E": {
      "step_1": -1.00000000,
      "step_2": -2.00000000,
      "step_3": -3.00000000,
      "step_4": -3.00000000,
      "step_5": -5.00000000,
      "step_6": -5.00000000,
      "step_7": -6.00000000,
      "step_8": -8.00000000,
      "step_9": -9.00000000,
      "step_10": -12.00000000,
      "step_11": -13.00000000,
      "step_12": -14.00000000,
      "step_13": -16.00000000,
      "step_14": -18.00000000,
      "step_15": -18.00000000,
      "step_16": -19.00000000,
      "step_17": -21.00000000,
      "step_18": -21.00000000,
      "step_19": -21.00000000,
      "step_20": -24.00000000,
      "step_21": -26.00000000,
      "step_22": -27.00000000,
      "step_23": -27.00000000,
      "step_24": -26.00000000,
      "step_25": -28.00000000,
      "step_26": -31.00000000,
      "step_27": -34.00000000,
      "step_28": -34.00000000,
      "step_29": -34.00000000
    }
  },
  "map_info": {
    "elevation_name": "aw3d_dem_30m",
    "projection": 99,
    "north": 661074.05043981,
    "south": 655044.05043981,
    "east": 384402.76720644,
    "west": 378372.76720644,
    "rows": 201,
    "cols": 201,
    "ewres": 30.00000000,
    "nsres": 30.00000000
  },
  "computation_parameters": {
    "easting": 381393.00000000,
    "northing": 658074.00000000,
    "search_m": 900.00,
    "search_cells": 30,
    "skip_m": 0.00,
    "skip_cells": 0,
    "flat_thresh_deg": 1.00000000,
    "flat_distance_m": 0.00,
    "flat_height_m": 0.00,
    "extended_correction": false
  },
  "intermediate_data": {
    "ternary_498": 0,
    "ternary_6561": 0,
    "pattern_size": 8,
    "origin_easting": 381387.76720644,
    "origin_northing": 658059.05043981,
    "origin_elevation_m": 149.00,
    "num_positives": 0,
    "num_negatives": 8,
    "pattern": {
      "NE": -1,
      "N": -1,
      "NW": -1,
      "W": -1,
      "SW": -1,
      "S": -1,
      "SE": -1,
      "E": -1
    },
    "rel_elevation_m": {
      "NE": -34.00,
      "N": -28.00,
      "NW": -27.00,
      "W": -18.00,
      "SW": -43.00,
      "S": -31.00,
      "SE": -37.00,
      "E": -18.00
    },
    "abs_elevation_m": {
      "NE": 115.00,
      "N": 121.00,
      "NW": 122.00,
      "W": 131.00,
      "SW": 106.00,
      "S": 118.00,
      "SE": 112.00,
      "E": 131.00
    },
    "distance_m": {
      "NE": 636.40,
      "N": 390.00,
      "NW": 254.56,
      "W": 270.00,
      "SW": 678.82,
      "S": 390.00,
      "SE": 424.26,
      "E": 420.00
    },
    "offset_easting_m": {
      "NE": 450.00,
      "N": 0.00,
      "NW": -180.00,
      "W": -270.00,
      "SW": -480.00,
      "S": 0.00,
      "SE": 300.00,
      "E": 420.00
    },
    "offset_northing_m": {
      "NE": 450.00,
      "N": 390.00,
      "NW": 180.00,
      "W": 0.00,
      "SW": -480.00,
      "S": -390.00,
      "SE": -300.00,
      "E": 0.00
    },
    "easting": {
      "NE": 381837.76720644,
      "N": 381387.76720644,
      "NW": 381207.76720644,
      "W": 381117.76720644,
      "SW": 380907.76720644,
      "S": 381387.76720644,
      "SE": 381687.76720644,
      "E": 381807.76720644
    },
    "northing": {
      "NE": 658509.05043981,
      "N": 658449.05043981,
      "NW": 658239.05043981,
      "W": 658059.05043981,
      "SW": 657579.05043981,
      "S": 657669.05043981,
      "SE": 657759.05043981,
      "E": 658059.05043981
    }
  },
  "final_results": {
    "landform_cat": 2,
    "landform_code": "PK",
    "landform_name": "peak",
    "landform_deviation": 0,
    "azimuth": 68.44430542,
    "elongation": 1.82631075,
    "width_m": 660.70,
    "intensity_m": 29.50,
    "exposition_m": 43.00,
    "range_m": 25.00,
    "variance": 66.75000000,
    "extends": 0.22764910,
    "octagon_perimeter_m": 3031.42,
    "octagon_area_m2": 521550.00,
    "mesh_perimeter_m": 3033.31,
    "mesh_area_m2": 523211.58
  },
  "format_version_major": 0,
  "format_version_minor": 9,
  "timestamp": "2020-12-14T17:16:43Z",
  "generator": "r.geomorphon GRASS GIS 7.9.dev (2020) [7d9a0da4f]"
}
```
