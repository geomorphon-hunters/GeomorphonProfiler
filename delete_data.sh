#!/bin/sh

# This file is a part of Geomorphon Profiler, a free software for geomorphon
# data processing. See the accompanying file "COPYING" for the copyright and
# licensing information.

# shellcheck source=./init.sh
. "$(dirname "$(realpath "$0")")/init.sh"
assert_nonempty_file "$INPUTFILE"

rm_allvis()
{
	rm -f "$BACKDROPDIR/$name.png" \
		"$CONTOURDIR/$name.png" \
		"$SHAPEAREADIR/$name.png" \
		"$SHAPELINESDIR/$name.png" \
		"$CIRCLEDIR/$name.png" \
		"$PNGDIR/$name.png"
}

what=${2:?}
ret=0
doing=0
TODO=$(wc -l < "$INPUTFILE")
perf_start
while read -r line; do
	name=$(echo "$line" | cut -d, -f1)
	doing=$((doing + 1))
	if [ -z "$name" ]; then
		$SQUELCH printf '%4u/%u: NULL\n' "$doing" "$TODO"
		continue
	fi
	$SQUELCH printf '%4u/%u: %s deleting... ' "$doing" "$TODO" "$name"
	case "$what" in
	profiles)
		rm -f "$JSONDIR/$name.json" || ret=1
		;;
	backdrops)
		rm -f "$BACKDROPDIR/$name.png" "$PNGDIR/$name.png" || ret=1
		;;
	contours)
		rm -f "$CONTOURDIR/$name.png" "$PNGDIR/$name.png" || ret=1
		;;
	shapes)
		rm -f "$SHAPEAREADIR/$name.png" "$SHAPELINESDIR/$name.png" \
			"$PNGDIR/$name.png" || ret=1
		;;
	circles)
		rm -f "$CIRCLEDIR/$name.png" "$PNGDIR/$name.png" || ret=1
		;;
	images)
		rm -f "$PNGDIR/$name.png" || ret=1
		;;
	ALLVIS)
		rm_allvis || ret=1
		;;
	ALL)
		rm_allvis || ret=1
		rm -f "$JSONDIR/$name.json" || ret=1
		;;
	*)
		echo "ERROR: '$what' is not a valid argument" >&2
		ret=2
		break
		;;
	esac
	if [ $ret -ne 0 ]; then
		echo 'ERROR: Failed to remove some files.' >&2
		break
	fi
	$SQUELCH echo 'done'
	perf_print $doing 100
done < "$INPUTFILE"
perf_print $doing 1
exit $ret
