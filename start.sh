#!/bin/sh

# This file is a part of Geomorphon Profiler, a free software for geomorphon
# data processing. See the accompanying file "COPYING" for the copyright and
# licensing information.

# shellcheck source=./init.sh
SHALLOW_INIT=yes . "$(dirname "$(realpath "$0")")/init.sh"
: "${EXPORT_SCRIPT:=$SCRIPTDIR/export.sh}"
THISFILE=$(basename "$0")

usage()
{
	cat <<ENDOFMESSAGE
Usage: $THISFILE [-d <what>] [-v] [-e]

Extract geomorphon profiles for every combination of the parameter values of
the matrix defined in config.sh.

Optional arguments:
  -d  instead of the processing delete existing data as follows:
      backdrops - the backdrop image cache (*)
      contours  - the contour lines cache (*)
      shapes    - the shape image cache (*)
      circles   - the superimposed circle image cache (*)
      images    - the complete geomorphon image PNG files, implicit for all of
                  the (*) options above
      ALLVIS    - all of the above
      profiles  - the geomorphon profile JSON files
      ALL       - all of the above
  -v  also visualize each geomorphon profile
  -e  also export custom data through '$EXPORT_SCRIPT' (must be supplied)
ENDOFMESSAGE
}

while getopts d:veh opt; do
	case "$opt" in
	v)
		VISUALIZE=yes
		;;
	e)
		VISUALIZE=yes
		EXPORT=yes
		;;
	d)
		DELETE=yes
		case "$OPTARG" in
		profiles|backdrops|contours|shapes|circles|images|ALLVIS|ALL)
			DELETEWHAT="$OPTARG"
			;;
		*)
			echo "ERROR: Invalid '$opt' argument, try $THISFILE -h" >&2
			exit 1
		esac
		;;
	h)
		usage
		exit 0
		;;
	*)
		echo "ERROR: Invalid arguments, try $THISFILE -h" >&2
		exit 1
		;;
	esac
done

delete_data()
{
	printf 'deleting %s data... ' "${2:?}"
	"$SCRIPTDIR/delete_data.sh" "${1:?}" "${2:?}"
}

run_round()
{
	: "${1:?}"
	"$SCRIPTDIR/extract_all_profiles.sh" "$1" || return 1
	if [ "$VISUALIZE" = "yes" ]; then
		"$SCRIPTDIR/visualize_all_profiles.sh" "$1" || return 1
	fi
	if [ "$EXPORT" = "yes" ]; then
		"$EXPORT_SCRIPT" "$1" || return 1
	fi
	return 0
}

validate_config_matrix()
{
	vcm_ret=0
	for se in $SEARCH; do
		validate_integer SEARCH "$se" || vcm_ret=1
	done
	for sk in $SKIP; do
		validate_integer SKIP "$sk" || vcm_ret=1
	done
	for fl in $FLAT; do
		validate_float FLAT "$fl" || vcm_ret=1
	done
	for di in $DIST; do
		validate_float DIST "$di" || vcm_ret=1
	done
	return $vcm_ret
}

formulate()
{
	FORM_TMPFILE=$(mktemp --tmpdir formula-XXXXXX.sh)
	form_ret=0
	form_done=0
	form_started0=$(date +%s)
	for se in $SEARCH; do
		for sk in $SKIP; do
			for fl in $FLAT; do
				for di in $DIST; do
					{
						echo "SEARCH=$se"
						echo "SKIP=$sk"
						echo "FLAT=$fl"
						echo "DIST=$di"
					} > "$FORM_TMPFILE"
					printf 'For SEARCH=%s SKIP=%s FLAT=%s DIST=%s: ' "$se" "$sk" "$fl" "$di"
					form_started=$(date +%s)
					if "${1:?}" "$FORM_TMPFILE" "$2"; then
						form_estr=$(format_elapsed_seconds $(($(date +%s) - form_started)))
						echo "done [$form_estr]"
					else
						echo 'ERROR!'
						form_ret=1
						break 4
					fi
					form_done=$((form_done + 1))
				done
			done
		done
	done
	rm -f "$FORM_TMPFILE"
	if [ "$form_done" -gt 1 ]; then
		form_estr=$(format_elapsed_seconds $(($(date +%s) - form_started0)))
		echo "Total permutations: $form_done, total time: $form_estr."
	fi
	return $form_ret
}

if ! validate_config_matrix; then
	echo "Terminating due to a configuration problem." >&2
	exit 1
fi
if [ "$DELETE" = "yes" ]; then
	formulate delete_data "$DELETEWHAT"
else
	formulate run_round
fi
