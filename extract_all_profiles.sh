#!/bin/sh

# This file is a part of Geomorphon Profiler, a free software for geomorphon
# data processing. See the accompanying file "COPYING" for the copyright and
# licensing information.

# shellcheck source=./init.sh
. "$(dirname "$(realpath "$0")")/init.sh"
require_grass_session
assert_nonempty_file "$INPUTFILE"
mkdir_or_fail "$JSONDIR"

TMPFILE=$(mktemp --tmpdir profile_XXXXXX.json)
ret=0
doing=0
TODO=$(wc -l < "$INPUTFILE")
perf_start
while read -r line; do
	name=$(echo "$line" | cut -d, -f1)
	coords=$(echo "$line" | cut -d, -f2-3)
	doing=$((doing + 1))
	targetfile="$JSONDIR"/$name.json
	$SQUELCH printf '%4u/%u: %s ' "$doing" "$TODO" "$targetfile"
	if [ -s "$targetfile" ]; then
		$SQUELCH echo 'already exists'
	else
		if [ -n "$SQUELCH" ] && [ -z "$REPORTED" ]; then
			printf 'extracting... '
			REPORTED=yes
		fi
		$SQUELCH echo -n 'extracting... '
		if ! "$SCRIPTDIR/extract_profile.py" \
			--elevation="$ELEVATION" \
			--visradius="$VIS_RADIUS" \
			--search="$SEARCH" \
			--skip="$SKIP" \
			--flat="$FLAT" \
			--dist="$DIST" \
			--coordinates="$coords" \
			--output="$TMPFILE"
		then
			echo 'ERROR: Worker script failed.' >&2
			ret=1
			break
		fi
		cp "$TMPFILE" "$targetfile" && $SQUELCH echo 'done'
	fi
	perf_print $doing 100
done < "$INPUTFILE"
perf_print $doing 1
rm -f "$TMPFILE"
exit $ret
